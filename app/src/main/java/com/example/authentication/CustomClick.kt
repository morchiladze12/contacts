package com.example.authentication

import com.example.authentication.account.ContactsModel
import com.example.authentication.account.UserModel


interface CustomClick {
    fun itemClick(model: ContactsModel, position:Int){}
    fun onClick(position:Int){}
}