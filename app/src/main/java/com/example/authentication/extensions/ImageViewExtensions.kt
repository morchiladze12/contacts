package com.example.authentication.extensions

import android.widget.ImageView
 import com.bumptech.glide.Glide
import com.example.authentication.tools.App

fun ImageView.setGlideImage(img: String) {
    Glide.with(App.context!!).load(img).into(this)
}