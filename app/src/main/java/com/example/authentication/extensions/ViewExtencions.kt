package com.example.authentication.extensions

import android.view.View

fun View.visible(boolean: Boolean){
    visibility = if (boolean) View.VISIBLE
    else View.GONE
}