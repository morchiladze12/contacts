package com.example.authentication.extensions

import android.widget.EditText
import androidx.core.content.ContextCompat


fun EditText.setValidColor(boolean: Boolean) {
    if (boolean)
        setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_light))
    else
        setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_light))
}
