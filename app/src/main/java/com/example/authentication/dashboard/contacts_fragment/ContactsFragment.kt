package com.example.authentication.dashboard.contacts_fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.authentication.*
import com.example.authentication.R
import com.example.authentication.account.ContactsModel
import com.example.authentication.dashboard.DashboardContactsActivity
import com.example.authentication.dashboard.contacts_fragment.add.AddContactActivity
import com.example.authentication.extensions.visible
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*


class ContactsFragment : BaseFragment() {
    override fun layoutResource() = R.layout.fragment_contacts

    private var contactsRV: RecyclerView? = null
    private var addContactFabButton: FloatingActionButton? = null
    lateinit var adapter: RecyclerViewAdapter
    var adapterPosition: Int? = null

    private val fb = FirebaseAuth.getInstance()
    private val contacts = mutableListOf<ContactsModel>()
    var da: DashboardContactsActivity? = null

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        da = activity as DashboardContactsActivity
        contactsRV = itemView!!.findViewById(R.id.contactsRV)
        addContactFabButton = itemView!!.findViewById(R.id.addContactFabButton)

        addContactFabButton!!.setOnClickListener {
            val intent = Intent(context, AddContactActivity::class.java)
            startActivity(intent)
        }

        if (fb.uid != null) {
            getContacts()
        }
        adapter = RecyclerViewAdapter(contacts, object : CustomClick {
            override fun itemClick(model: ContactsModel, position: Int) {
                FirebaseDatabase.getInstance().getReference("/users/${fb.uid}/contacts")
                    .child(model.number).removeValue()
                contacts.removeAt(position)
                adapter.notifyItemRemoved(position)
                adapterPosition = position

            }

            override fun onClick(position: Int) {
                AddContactActivity.isEditContact = true
                val intent = Intent(context, AddContactActivity::class.java)
                intent.putExtra("name", contacts[position].name)
                intent.putExtra("number", contacts[position].number)
                intent.putExtra("image", contacts[position].imageUrl)
                startActivity(intent)
                adapterPosition = position
            }
        })
        contactsRV!!.layoutManager = LinearLayoutManager(context)
        contactsRV!!.adapter = adapter
    }

    private fun getContacts() {
        itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)
        val uid = FirebaseAuth.getInstance().uid ?: ""

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("/users/$uid/contacts")

        myRef.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val contactModel = snapshot.getValue(ContactsModel::class.java)!!
                contacts.add(contactModel)
                adapter.notifyDataSetChanged()
                itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val contactModel = snapshot.getValue(ContactsModel::class.java)!!
                itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                contacts.removeAt(adapterPosition!!)
                contacts.add(adapterPosition!!, contactModel)
                adapter.notifyItemChanged(adapterPosition!!)
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                adapter.notifyItemRemoved(adapterPosition!!)
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
                itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
            }

        })
        itemView!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
    }


}