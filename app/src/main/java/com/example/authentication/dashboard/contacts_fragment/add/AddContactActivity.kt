package com.example.authentication.dashboard.contacts_fragment.add

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.authentication.CustomClick
import com.example.authentication.R
import com.example.authentication.account.ContactsModel
import com.example.authentication.extensions.setGlideImage
import com.example.authentication.extensions.visible
import com.example.authentication.tools.App.Companion.context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class AddContactActivity : AppCompatActivity() {

    var addImageIV: ImageView? = null
    var saveContactButton: Button? = null
    var cancelContactButton: Button? = null
    var circleProfileImage: CircleImageView? = null
    var contactNameET: EditText? = null
    var contactNumberET: EditText? = null
    var addContactFromPhoneButton: Button? = null
    private var selectedPhotoUri: Uri? = null
    private var selectedPhotoUriString: String? = null
    private lateinit var storage: FirebaseStorage
    private lateinit var adapter: PhoneContactsRVAdapter

    companion object {
        var isEditContact = false
        var isEditContactSave = false
    }

    private var phoneContacts = mutableListOf<ContactsModel>()
    var name: String? = null
    var number: String? = null
    var image: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contact)

        saveContactButton = findViewById(R.id.saveContactButton)
        addContactFromPhoneButton = findViewById(R.id.addContactFromPhoneButton)
        addImageIV = findViewById(R.id.addImageIV)
        cancelContactButton = findViewById(R.id.cancelContactButton)
        circleProfileImage = findViewById(R.id.circleProfileImage)
        contactNameET = findViewById(R.id.contactNameET)
        contactNumberET = findViewById(R.id.contactNumberET)
        storage = FirebaseStorage.getInstance()



        initClick()


        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_CONTACTS), 100
            )
        } else {
            getContactList()
        }
        addContactFromPhoneButton!!.setOnClickListener {
            initContactsDialog(this, phoneContacts)
        }

        if (isEditContact) {
            name = intent.extras!!.getString("name")
            number = intent.extras!!.getString("number")
            image = intent.extras!!.getString("image")
            contactNameET!!.setText(name)
            contactNumberET!!.setText(number)
            circleProfileImage!!.setGlideImage(image!!)
            isEditContactSave = true
            selectedPhotoUriString = image
            isEditContact = false

        }
    }

    private fun initClick() {
        saveContactButton!!.setOnClickListener {
            val contact = ContactsModel()
            contact.number = contactNumberET!!.text.toString()
            contact.name = contactNameET!!.text.toString()
            contact.imageUrl = selectedPhotoUri.toString()

            if (contact.name.isEmpty()) {
                contactNameET!!.error = "Enter Name"
                contactNameET!!.requestFocus()
                return@setOnClickListener
            }
            if (contact.number.isEmpty()) {
                contactNumberET!!.error = "Enter Number"
                contactNumberET!!.requestFocus()
                return@setOnClickListener
            }

            if (selectedPhotoUri == null) {
                addContact(contact)
                finish()
            } else {
                uploadImageToFirebaseStorage(contact)
            }


        }
        cancelContactButton!!.setOnClickListener {
            finish()
        }

        circleProfileImage!!.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/+"
            startActivityForResult(intent, 0)
        }
    }

    private fun editContact(model: ContactsModel, number: String) {
        findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)
        val database = FirebaseDatabase.getInstance()
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val myRef = database.getReference("/users/$uid/contacts/$number")
        myRef.setValue(model).addOnSuccessListener {
            findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
            finish()
        }
        findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
        isEditContactSave = false
    }

    private fun addContact(model: ContactsModel) {
        findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)
        if (isEditContactSave) {
            editContact(model, number!!)
        } else {
            val uid = FirebaseAuth.getInstance().uid ?: ""
            val ref =
                FirebaseDatabase.getInstance().getReference("/users/$uid/contacts/${model.number}")

            ref.setValue(model)
                .addOnSuccessListener {
                    // success
                    findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                    finish()
                }.addOnFailureListener {
                    Toast.makeText(this, "failed", Toast.LENGTH_LONG).show()
                    findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {

            selectedPhotoUri = data.data
            selectedPhotoUriString = selectedPhotoUri.toString()
            val bitmap =
                MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            circleProfileImage!!.setImageBitmap(bitmap)
        }

    }


    private fun uploadImageToFirebaseStorage(model: ContactsModel) {
        if (selectedPhotoUri == null) return
        findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    addContact(model)
                }
            }
            .addOnFailureListener {
                findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        getContactList()
    }

    private fun getContactList() {
        val cr: ContentResolver = contentResolver
        val cur: Cursor? = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if ((cur?.count ?: 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                val id: String = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID)
                )
                val name: String = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )
                if (cur.getInt(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER
                        )
                    ) > 0
                ) {
                    val pCur: Cursor? = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    val contactModel = ContactsModel()
                    while (pCur!!.moveToNext()) {
                        val phoneNo: String = pCur.getString(
                            pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                            )
                        )
                        contactModel.name = name
                        contactModel.number = phoneNo
                        phoneContacts.add(contactModel)
//                        contacts.add(
//                            ContactModel(
//                                phoneNo,
//                                name,
//                                selectedPhotoBitmap
//                            )
//                        )
                    }
                    pCur.close()
                }
            }
        }
        cur?.close()
    }

    private fun setContact(model: ContactsModel) {
        contactNameET!!.setText(model.name)
        contactNumberET!!.setText(model.number)
    }

    private fun initContactsDialog(context: Context, phoneNum: MutableList<ContactsModel>) {
        val dialog = Dialog(context)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.contacts_dialog_layout)

        val phoneContactsRV = dialog.findViewById<RecyclerView>(R.id.phoneContactsRV)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams

        adapter = PhoneContactsRVAdapter(phoneNum, object : CustomClick {
            override fun onClick(position: Int) {
                setContact(phoneNum[position])
                dialog.dismiss()
            }
        })
        phoneContactsRV!!.layoutManager = LinearLayoutManager(this)
        phoneContactsRV.adapter = adapter

        dialog.show()
    }

}