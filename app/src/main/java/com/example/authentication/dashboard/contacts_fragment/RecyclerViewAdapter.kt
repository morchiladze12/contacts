package com.example.authentication.dashboard.contacts_fragment


import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.authentication.CustomClick
import com.example.authentication.R
import com.example.authentication.account.ContactsModel
import com.example.authentication.account.UserModel
import com.example.authentication.extensions.setGlideImage
import de.hdodenhof.circleimageview.CircleImageView

class RecyclerViewAdapter(
    private val contact: MutableList<ContactsModel>,
    private val customClick: CustomClick
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.contacts_rv_layout, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = contact.size

    private var imageView: CircleImageView? = null
    private var numberTextView: TextView? = null
    private var nameTV: TextView? = null
    private var editButton: ImageView? = null
    private var deleteButton: ImageView? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: ContactsModel

        fun onBind() {
            model = contact[adapterPosition]

            imageView = itemView.findViewById(R.id.contactImageView)
            numberTextView = itemView.findViewById(R.id.numberTV)
            nameTV = itemView.findViewById(R.id.nameTV)
            editButton = itemView.findViewById(R.id.editButton)
            deleteButton = itemView.findViewById(R.id.deleteButton)

            nameTV!!.text = model.name
            numberTextView!!.text = model.number

            if (model.imageUrl != null) {
                imageView!!.setGlideImage(model.imageUrl!!.toString())
            } else {
                imageView!!.setImageResource(R.drawable.ic_baseline_account_circle_24)
            }

            deleteButton!!.setOnClickListener {
                customClick.itemClick(model, adapterPosition)
            }

            editButton!!.setOnClickListener {
                customClick.onClick(adapterPosition)
            }
        }
    }
}