package com.example.authentication.dashboard.contacts_fragment.add

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.authentication.CustomClick
import com.example.authentication.R
import com.example.authentication.account.ContactsModel
import com.example.authentication.dashboard.contacts_fragment.RecyclerViewAdapter
import com.example.authentication.extensions.visible
import de.hdodenhof.circleimageview.CircleImageView

class PhoneContactsRVAdapter(
    private val contact: MutableList<ContactsModel>,
    private val customClick: CustomClick
) :
    RecyclerView.Adapter<PhoneContactsRVAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.contacts_rv_layout, parent, false)
    )

    override fun onBindViewHolder(holder: PhoneContactsRVAdapter.ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = contact.size

    private var imageView: CircleImageView? = null
    private var numberTextView: TextView? = null
    private var nameTV: TextView? = null
    private var editButton: ImageView? = null
    private var deleteButton: ImageView? = null


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: ContactsModel
        fun onBind() {
            model = contact[adapterPosition]

            imageView = itemView.findViewById(R.id.contactImageView)
            numberTextView = itemView.findViewById(R.id.numberTV)
            nameTV = itemView.findViewById(R.id.nameTV)
            editButton = itemView.findViewById(R.id.editButton)
            deleteButton = itemView.findViewById(R.id.deleteButton)

            nameTV!!.text = model.name
            numberTextView!!.text = model.number

            editButton!!.visible(false)
            deleteButton!!.visible(false)

            itemView.setOnClickListener {
                customClick.onClick(adapterPosition)
            }
        }
    }


}