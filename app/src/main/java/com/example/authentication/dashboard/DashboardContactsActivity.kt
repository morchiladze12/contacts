package com.example.authentication.dashboard

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.example.authentication.R
import com.example.authentication.account.login.LogInActivity
import com.example.authentication.dashboard.contacts_fragment.ContactsFragment
import com.example.authentication.dashboard.profile_fragment.ProfileFragment
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth


class DashboardContactsActivity : AppCompatActivity() {

    private lateinit var viewPagerAdapter: DashboardViewPagerAdapter
    private var contactsTabLayout: TabLayout? = null
    var contactsViewPager: ViewPager? = null
     var handler: Handler? = null
    var run: Runnable? = null
    private lateinit var auth: FirebaseAuth
    private val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_contacts)
        contactsTabLayout = findViewById(R.id.contactsTabLayout)
        contactsViewPager = findViewById(R.id.contactsViewPager)
         auth = FirebaseAuth.getInstance()

        if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
            initTabLayout()
        }
        handler = Handler()
        run = Runnable {
            auth.signOut()
            val intent = Intent(this,LogInActivity::class.java)
            startActivity(intent)
            finish()
        }
        startHandler()
    }

    private fun checkPermissionREAD_EXTERNAL_STORAGE(
        context: Context?
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (context as Activity?)!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    showDialog(
                        "External storage", context,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                } else {
                    ActivityCompat
                        .requestPermissions(
                            (context as Activity?)!!,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                        )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    private fun showDialog(
        msg: String, context: Context?,
        permission: String
    ) {
        val alertBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
        alertBuilder.setCancelable(true)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            ActivityCompat.requestPermissions(
                (context as Activity?)!!, arrayOf(permission),
                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
            )
        }
        val alert: AlertDialog = alertBuilder.create()
        alert.show()
    }

    override fun onDestroy() {
        auth.signOut()
        super.onDestroy()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initTabLayout()
            } else {
                Toast.makeText(
                    this@DashboardContactsActivity, "GET_ACCOUNTS Denied",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> super.onRequestPermissionsResult(
                requestCode, permissions!!,
                grantResults
            )
        }
    }

    private fun initTabLayout() {
        val fragmentList = listOf<Fragment>(
            ContactsFragment(),
            ProfileFragment()
        )
        viewPagerAdapter = DashboardViewPagerAdapter(fragmentList, supportFragmentManager)
        contactsViewPager!!.adapter = viewPagerAdapter
        contactsTabLayout!!.setupWithViewPager(contactsViewPager)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        stopHandler()
        startHandler()
    }

    private fun stopHandler() {
        handler!!.removeCallbacks(run!!)
    }

    private fun startHandler() {
        handler!!.postDelayed(run!!, 3 * 60 * 1000)
    }

}