package com.example.authentication.dashboard.profile_fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.authentication.BaseFragment
import com.example.authentication.R
import com.example.authentication.account.UserModel
import com.example.authentication.account.login.LogInActivity
import com.example.authentication.dashboard.DashboardContactsActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : BaseFragment() {

    override fun layoutResource() = R.layout.fragment_profile

    private var signOutButton: Button? = null
    private var profileNameTV: TextView? = null
    private var profileEmailTv: TextView? = null
    private var profileNumberTV: TextView? = null
    private lateinit var auth: FirebaseAuth
    var da: DashboardContactsActivity? = null


    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        da = activity as DashboardContactsActivity?
        auth = FirebaseAuth.getInstance()
        signOutButton = itemView!!.findViewById(R.id.signOutButton)
        profileNameTV = itemView!!.findViewById(R.id.profileNameTV)
        profileEmailTv = itemView!!.findViewById(R.id.profileEmailTV)
        profileNumberTV = itemView!!.findViewById(R.id.profileNumberTV)

        signOutButton!!.setOnClickListener {
            auth.signOut()
            val intent = Intent(da, LogInActivity::class.java)
            startActivity(intent)
            da!!.finish()
        }

        getUserProfile()
    }


    private fun getUserProfile() {
        val uid = FirebaseAuth.getInstance().uid ?: ""

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("/users/$uid")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(UserModel::class.java)

                profileNameTV!!.text = value!!.name + " " + value!!.surname
                profileEmailTv!!.text = value.email
                profileNumberTV!!.text = value.phoneNumber
                d("vavavav", value.name)
                d("vavavav", value.email)

            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("onDataChange", "Failed to read value.", error.toException())
            }
        })
    }


}