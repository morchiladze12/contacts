package com.example.authentication.account.sign_up

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.authentication.R
import com.example.authentication.account.UserModel
import com.example.authentication.tools.Tools
import com.example.authentication.extensions.visible
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import java.util.concurrent.TimeUnit

class VerifyPhoneFragment(private val userModel: UserModel) :
    Fragment(R.layout.fragment_verify_phone) {

    private var ma: SignUpActivity? = null
    private var editTextCode: EditText? = null
    private var progressBar: ProgressBar? = null

    private var verificationId: String? = null
    private var mAuth: FirebaseAuth? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ma = activity as SignUpActivity?
        editTextCode = view.findViewById(R.id.editTextCode)
        progressBar = view.findViewById(R.id.progressbar)
        mAuth = FirebaseAuth.getInstance()



        d("phonenumber", userModel.phoneNumber)
        sendVerificationCode(userModel.phoneNumber)

        // save phone number

        // save phone number
        val prefs: SharedPreferences = ma!!.applicationContext.getSharedPreferences(
            "USER_PREF",
            Context.MODE_PRIVATE
        )
        val editor = prefs.edit()
        editor.putString("phoneNumber", userModel.phoneNumber)
        editor.apply()

        view.findViewById<View>(R.id.buttonSignIn).setOnClickListener {
            val code: String = editTextCode!!.text.toString().trim()
            if (code.isEmpty() || code.length < 6) {
                editTextCode!!.error = "Enter code..."
                return@setOnClickListener
            }
            d("codeee", code)
            verifyCode(code)

        }

    }

    private fun verifyCode(code: String) {
        val credential = PhoneAuthProvider.getCredential(verificationId!!, code)
        signInWithCredential(credential)
    }

    private fun signInWithCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    progressBar!!.visibility = View.GONE
                    signUp()
                } else {
                    Toast.makeText(
                        ma,
                        task.exception!!.message,
                        Toast.LENGTH_LONG
                    ).show()
                    progressBar!!.visibility = View.GONE
                }
            }
    }

    private fun sendVerificationCode(number: String) {
        progressBar!!.visibility = View.VISIBLE
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            number,
            60,
            TimeUnit.SECONDS,
            ma!!,
            mCallBack
        )
    }

    private val mCallBack: OnVerificationStateChangedCallbacks =
        object : OnVerificationStateChangedCallbacks() {
            override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken) {
                super.onCodeSent(s, forceResendingToken)
                verificationId = s
                d("codeee2", verificationId.toString())
                progressBar!!.visibility = View.GONE

            }

            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                val code = phoneAuthCredential.smsCode
                if (code != null) {
                    editTextCode!!.setText(code)
                    verifyCode(code)
                    progressBar!!.visibility = View.GONE
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(ma, e.message, Toast.LENGTH_LONG).show()
                progressBar!!.visibility = View.GONE
            }
        }


    private fun signUp() {
        view!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)
        ma!!.auth.createUserWithEmailAndPassword(
            userModel.email,
            userModel.password,
        )
            .addOnCompleteListener(ma!!) { task ->
                if (task.isSuccessful) {
                    ma!!.saveUserToFirebaseDatabase(userModel)
                } else {
                    view!!.findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                    Tools.initDialog(
                        ma!!,
                        getString(R.string.registration_failed),
                        getString(R.string.please_try_again)
                    )
                }

            }
    }
}