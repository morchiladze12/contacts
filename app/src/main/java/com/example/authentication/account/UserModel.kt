package com.example.authentication.account

import android.net.Uri


class UserModel{
    var uid: String = ""
    var email: String = ""
    var password: String = ""
    var name: String = ""
    var surname: String = ""
    var phoneNumber: String = ""
    var profileImageUrl: String = ""
//    var contacts = mutableListOf<ContactsModel>()


}

class ContactsModel{
    var imageUrl:String? = null
    var name:String = ""
    var number:String = ""
}

