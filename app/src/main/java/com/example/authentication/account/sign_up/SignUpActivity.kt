package com.example.authentication.account.sign_up

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.authentication.R
import com.example.authentication.account.ContactsModel
import com.example.authentication.account.UserModel
import com.example.authentication.account.login.LogInActivity
import com.example.authentication.dashboard.DashboardContactsActivity
import com.example.authentication.extensions.setValidColor
import com.example.authentication.tools.Tools
import com.example.authentication.extensions.visible
import com.google.firebase.auth.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import java.util.*

class SignUpActivity : AppCompatActivity() {
    private var selectedPhotoUri: Uri? = null
    lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var storage: FirebaseStorage
    private lateinit var binding: com.example.authentication.databinding.ActivitySignUpBinding
    private lateinit var viewModel: SignUpViewModel


    private var verificationInProgress = false
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)



        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference
        storage = FirebaseStorage.getInstance()
        viewModel = ViewModelProvider(this)[SignUpViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        init()

    }

    private fun init() {
        binding.signUpButton.setOnClickListener {
//            signUp()
           initSignUp()
        }


        binding.textLogIn.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
//        binding.circleProfileImage.setOnClickListener {
//            val intent = Intent(Intent.ACTION_PICK)
//            intent.type = "image/+"
//            startActivityForResult(intent, 0)
//        }
        setObservers()
    }


    private fun initSignUp(){
        val code: String = binding.editTextCountryCode.text.toString().trim()
        val number: String = binding.userNumberSignUpEditText.text.toString().trim()
        val name: String = binding.nameSignUpEditText.text.toString()
        val surname: String = binding.surnameSignUpEditText.text.toString()
        val email: String = binding.emailSignUpEditText.text.toString()
        val password: String = binding.passwordSignUpEditText.text.toString()


        if(name.isEmpty()){
            binding.nameSignUpEditText.error = "Enter Name"
            binding.nameSignUpEditText.requestFocus()
            return
        }
        if(surname.isEmpty()){
            binding.surnameSignUpEditText.error = "Enter Surname"
            binding.surnameSignUpEditText.requestFocus()
            return
        }
        if (number.isEmpty() || number.length != 9) {
            binding.userNumberSignUpEditText.error = "Valid number is required"
            binding.userNumberSignUpEditText.requestFocus()
            return
        }

        if (!Tools.isEmailValid(viewModel.emailLiveData.value!!) ){
            binding.emailSignUpEditText.error = "Invalid Email"
            binding.emailSignUpEditText.requestFocus()
            return
        }
        if (!Tools.passwordValidation(viewModel.passwordLiveData.value!!) ){
            binding.passwordSignUpEditText.error = "Invalid Password"
            binding.passwordSignUpEditText.requestFocus()
            return
        }

        val userModel = UserModel()
        userModel.name = name
        userModel.surname = surname
        userModel.phoneNumber  = code + number
        userModel.email = email
        userModel.password = password
        userModel.profileImageUrl = ""

        findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)
        auth.createUserWithEmailAndPassword(
            userModel.email,
            userModel.password,
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                     saveUserToFirebaseDatabase(userModel)
                } else {
                     findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                    Tools.initDialog(
                        this,
                        getString(R.string.registration_failed),
                        getString(R.string.please_try_again)
                    )
                }

            }
//        setFragment(VerifyPhoneFragment(userModel),false)
    }


    private fun signUp() {


    }


    private fun setFragment(fragment: Fragment, back: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
        if (back) {
            transaction.setCustomAnimations(
                R.anim.enter_fade_in_left,
                R.anim.exit_fade_out_right,
                R.anim.enter_fade_in_right,
                R.anim.exit_fade_out_left
            )
        } else transaction.setCustomAnimations(
            R.anim.enter_fade_in_right,
            R.anim.exit_fade_out_left,
            R.anim.enter_fade_in_left,
            R.anim.exit_fade_out_right
        )
        findViewById<FrameLayout>(R.id.fragmentContainer).visibility = View.VISIBLE

        supportFragmentManager.beginTransaction().apply {
            add(R.id.fragmentContainer, fragment)
            addToBackStack(null)
            commit()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            //    binding.circleProfileImage.setImageBitmap(bitmap)
        }

    }

    fun saveUserToFirebaseDatabase(model: UserModel) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        ref.setValue(model)
            .addOnSuccessListener {
                val intent = Intent(this, DashboardContactsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                startActivity(intent)
            }.addOnFailureListener {
                findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                Toast.makeText(this,"failed",Toast.LENGTH_LONG).show()
            }
    }

    private fun setObservers() {
        viewModel.emailLiveData.observe(this, {
            binding.emailSignUpEditText.setValidColor(Tools.isEmailValid(it))
        })
        viewModel.passwordLiveData.observe(this, {
            binding.passwordSignUpEditText.setValidColor(Tools.passwordValidation(it))
        })
    }


}