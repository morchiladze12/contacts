package com.example.authentication.account.login

import android.content.Intent
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.authentication.R
import com.example.authentication.account.sign_up.SignUpActivity
import com.example.authentication.dashboard.DashboardContactsActivity
import com.example.authentication.databinding.ActivityLogInBinding
import com.example.authentication.extensions.setValidColor
import com.example.authentication.tools.Tools
import com.example.authentication.extensions.visible
import com.google.firebase.auth.*


class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: LogInViewModel
    private lateinit var binding: ActivityLogInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_log_in)
        viewModel = ViewModelProvider(this)[LogInViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        auth = FirebaseAuth.getInstance()
        init()

    }

    private fun init() {
        setObservers()
        binding.logInButton.setOnClickListener {
            logIn()
        }

        binding.textSignUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    private fun logIn() {
        if (Tools.isEmailValid(viewModel.emailLiveData.value!!) && Tools.passwordValidation(
                viewModel.passwordLiveData.value!!
            )
        ) {
            findViewById<FrameLayout>(R.id.spinKitContainer).visible(true)
            auth.signInWithEmailAndPassword(
                viewModel.emailLiveData.value!!,
                viewModel.passwordLiveData.value!!
            )
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, DashboardContactsActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                        startActivity(intent)

                    } else {
                        findViewById<FrameLayout>(R.id.spinKitContainer).visible(false)
                        Toast.makeText(
                            baseContext, "password or email is incorrect",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        } else {
            Tools.initDialog(
                this,
                getString(R.string.username_or_password_not_valid),
                getString(R.string.please_enter_valid)
            )
        }

    }


    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null) {
            auth.signOut()
        }
    }

    private fun setObservers() {
        viewModel.emailLiveData.observe(this, {
            binding.emailLogInEditText.setValidColor(Tools.isEmailValid(it))
        })
        viewModel.passwordLiveData.observe(this, {
            binding.passwordLogInEditText.setValidColor(Tools.passwordValidation(it))
        })
    }


}