package com.example.authentication.account.sign_up

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SignUpViewModel : ViewModel() {
    val emailLiveData by lazy {
        MutableLiveData<String>()
    }
    val passwordLiveData by lazy {
        MutableLiveData<String>()
    }

//    val userNameLiveData by lazy {
//        MutableLiveData<String>()
//    }
//    val userSurnameLiveData by lazy {
//        MutableLiveData<String>()
//    }

    init {
        emailLiveData.value = ""
        passwordLiveData.value = ""
//        repeatPasswordLiveData.value = ""
//        userNameLiveData.value = ""
    }
}