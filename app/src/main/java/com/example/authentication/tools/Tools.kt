package com.example.authentication.tools

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.util.Patterns
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.example.authentication.R
import java.text.SimpleDateFormat
import java.util.*

object Tools {
    fun dpToPx(dp: Int): Float {
        return (dp * Resources.getSystem().displayMetrics.density)
    }

    fun isEmailValid(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun passwordValidation(password: String) = (password.length > 5)

    fun initDialog(context: Context, title: String, description: String) {
        val dialog = Dialog(context)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.error_dialog_layout)

        val errorDialogTitleTV = dialog.findViewById<TextView>(R.id.errorDialogTitleTV)
        val errorDialogDescriptionTV = dialog.findViewById<TextView>(R.id.errorDialogDescriptionTV)
        val errorDialogButton = dialog.findViewById<TextView>(R.id.errorDialogButton)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams
        errorDialogTitleTV.text = title
        errorDialogDescriptionTV.text = description
        errorDialogButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}

