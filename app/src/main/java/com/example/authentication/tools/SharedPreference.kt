package com.example.authentication.tools

import android.content.Context
import android.content.SharedPreferences

class SharedPreference {
    companion object {
        const val USERNAME = "USERNAME"
        const val PASSWORD = "PASSWORD"
        const val NUMBER = "NUMBER"
        const val NAME = "NAME"
        const val IMAGE = "IMAGE"
        const val SURNAME = "SURNAME"
        const val TIMEOUT = "TIMEOUT"


        private var sharedPreference: SharedPreference? = null
        fun instance(): SharedPreference {
            if (sharedPreference == null) {
                sharedPreference =
                    SharedPreference()
            }
            return sharedPreference!!
        }
    }


    private val sharedPreferences: SharedPreferences by lazy {
        App.context!!.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    fun saveString(key:String,value:String){
        editor.putString(key,value)
        editor.apply()
    }

    fun getString(key: String) = sharedPreferences.getString(key,"")

    fun clear(): SharedPreferences.Editor = editor.clear()

    fun delete(key: String){
        if (sharedPreferences.contains(key)){
            editor.remove(key)
        }
        editor.apply()
    }
}